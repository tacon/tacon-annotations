package dev.tacon.annotations;

/**
 * Indicates that the annotated element must not be {@code null}.
 *
 * <p>This annotation is primarily used to convey the intent of the programmer, and
 * can be used by tools to identify and prevent potential issues related to null
 * values at compile-time.</p>
 *
 * <p>Usage examples:</p>
 *
 * <pre>
 * {@code
 * public class MyClass {
 *
 * 	// This method should never return a null value @NonNull
 * 	public String myMethod() {
 * 		return "Hello, World!";
 * 	}
 *
 * 	// This method expects a non-null parameter
 * 	public void anotherMethod( @NonNull String input) {
 * 		// ... method body ...
 * 	}
 * }
 * }
 * </pre>
 *
 * <strong>NOTE:</strong> While this annotation indicates the intent, it doesn't
 * enforce any runtime checks. Runtime checks for nullability should be handled
 * separately if needed.
 */
public @interface NonNull {}