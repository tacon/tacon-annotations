package dev.tacon.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Indicates that the elements within the annotated scope (method, type, or package) should be treated
 * as if they are non-null by default. Elements in the scope that should be nullable must be
 * explicitly annotated with the {@linkplain Nullable} annotation.
 *
 * <p>This annotation is typically used to reduce the number of non-null annotations
 * needed in the code, making the intent clearer by highlighting only the exceptions
 * where nullable values are allowed.</p>
 *
 * <p>Usage examples:</p>
 *
 * <pre>
 * {@code @NonNullByDefault
 * public class MyClass {
 *
 * 	// This method is considered to have a non-null return type by default.
 * 	public String myMethod() {
 * 		return "Hello, World!";
 * 	}
 * }
 * }
 * </pre>
 */
@Target({ ElementType.METHOD, ElementType.TYPE, ElementType.PACKAGE })
public @interface NonNullByDefault {}