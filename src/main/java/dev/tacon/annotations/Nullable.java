package dev.tacon.annotations;

/**
 * Indicates that the annotated element can be {@code null}.
 *
 * <p>This annotation is used to express the programmer's intent that the annotated element
 * might be null, and that callers should be prepared to handle such a scenario. It can also
 * be used by static analysis tools to identify potential issues or to suppress warnings
 * related to nullability.</p>
 *
 * <p>Usage examples:</p>
 *
 * <pre>
 * {@code
 * public class MyClass {
 *
 * 	// This method might return a null value @Nullable
 * 	public String getDescription() {
 * 		return (someCondition) ? "Description" : null;
 * 	}
 *
 * 	// This method is prepared to handle a null parameter
 * 	public void processValue( @Nullable String value) {
 * 		if (value != null) {
 * 			// ... handle value ...
 * 		} else {
 * 			// ... handle null scenario ...
 * 		}
 * 	}
 * }
 * }
 * </pre>
 *
 * <strong>NOTE:</strong> While this annotation indicates the intent, it doesn't
 * enforce any runtime checks. Runtime checks for nullability should be handled
 * separately if needed.
 */
public @interface Nullable {}